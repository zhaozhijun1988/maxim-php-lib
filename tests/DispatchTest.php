<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use Mojomaja\Component\Maxim\Client;
use Mojomaja\Component\Maxim\Message;

class DispatchTest extends PHPUnit_Framework_TestCase
{
    public function testDispatchOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/message/send'),
                $this->equalTo([
                    'receiver_id'   => 113,
                    'receiver_type' => Client::TYPE_USER,
                    'text'          => 'hello',
                    'token'         => 'nB5yFBnEEeSf7wgAJyXQOcVYCrc='
                ])
            )
            ->will($this->returnValue(json_encode([ 'error' => 0 ])))
        ;

        $maxim  = new Client('http://example.com', $skurl);
        $resp   = $maxim->dispatch(new Message([
            'text'  => 'hello',
            'token' => 'nB5yFBnEEeSf7wgAJyXQOcVYCrc='
        ]), 113);
        $this->assertEquals([ 'error' => 0 ], $resp);
    }
}
