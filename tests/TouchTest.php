<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use Mojomaja\Component\Maxim\Client;

class TouchTest extends PHPUnit_Framework_TestCase
{
    public function testTouchOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/temp/relation'),
                $this->equalTo([
                    'user_id'   => 97,
                    'token'     => 'nB5yFBnEEeSf7wgAJyXQOcVYCrc='
                ])
            )
            ->will($this->returnValue(json_encode([ 'error' => 0 ])))
        ;

        $maxim  = new Client('http://example.com', $skurl);
        $resp   = $maxim->touch(97, 'nB5yFBnEEeSf7wgAJyXQOcVYCrc=');
        $this->assertEquals([ 'error' => 0 ], $resp);
    }
}
