<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use Mojomaja\Component\Maxim\Client;
use Mojomaja\Component\Maxim\Message;

class ChallengeTest extends PHPUnit_Framework_TestCase
{
    public function testChallengeOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/temp/chat'),
                $this->equalTo([
                    'receiver_ids'  => '2,3',
                    'text'          => 'hello',
                    'picture'       => '@da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg',
                    'reference_id'  => 97,
                    'token'         => 'nB5yFBnEEeSf7wgAJyXQOcVYCrc='
                ])
            )
            ->will($this->returnValue(json_encode([ 'error' => 0 ])))
        ;

        $maxim  = new Client('http://example.com', $skurl);
        $resp   = $maxim->challenge([2, 3], new Message([
            'text'  => 'hello',
            'image' => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg',
            'token' => 'nB5yFBnEEeSf7wgAJyXQOcVYCrc='
        ]), 97);
        $this->assertEquals([ 'error' => 0 ], $resp);
    }
}
