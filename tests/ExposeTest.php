<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use Mojomaja\Component\Maxim\Client;

class ExposeTest extends PHPUnit_Framework_TestCase
{
    public function testExposeOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/friend/recommend'),
                $this->equalTo([
                    'data'  => json_encode([[
                        'user_id'   => 113,
                        'contacts'  => [
                            [ 'id' => 2, 'name' => 'bob',   'mobile' => null ],
                            [ 'id' => 3, 'name' => 'carli', 'mobile' => null ]
                        ]
                    ]])
                ])
            )
            ->will($this->returnValue(json_encode([ 'error' => 0 ])))
        ;

        $maxim  = new Client('http://example.com', $skurl);
        $resp   = $maxim->expose(113, [
            [ 2, 'bob' ],
            [ 3, 'carli' ]
        ]);
        $this->assertEquals([ 'error' => 0 ], $resp);
    }
}
