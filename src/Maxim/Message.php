<?php

namespace Mojomaja\Component\Maxim;

class Message
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $audio;

    /**
     * @var array
     */
    private $meta;

    /**
     * @var string
     */
    private $token;


    public function __construct(array $articles = [])
    {
        foreach ($articles as $k => $v) {
            $f = 'set'.ucfirst($k);
            if (method_exists($this, $f))
                $this->$f($v);
        }
    }

    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string    $image  file path
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string    $audio  file path
     */
    public function setAudio($audio)
    {
        $this->audio = $audio;

        return $this;
    }

    public function getAudio()
    {
        return $this->audio;
    }

    public function setMeta(array $meta = null)
    {
        $this->meta = $meta;

        return $this;
    }

    public function getMeta()
    {
        return $this->meta;
    }

    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function normalize()
    {
        return array_filter([
            'text'      => $this->text,
            'picture'   => $this->image ? "@{$this->image}" : null,
            'audio'     => $this->audio ? "@{$this->audio}" : null,
            'meta'      => $this->meta  ? json_encode($this->meta) : null,
            'token'     => $this->token
        ]);
    }
}
