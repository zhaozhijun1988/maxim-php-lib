<?php

namespace Mojomaja\Component\Maxim;

class Client
{
    const TYPE_USER     = 1;
    const TYPE_GROUP    = 2;

    /**
     * @var string
     */
    private $gateway;

    /**
     * @var \Mojomaja\Component\Skurl\Client
     */
    private $skurl;


    public function __construct($gateway, \Mojomaja\Component\Skurl\Client $skurl)
    {
        $this->gateway  = $gateway;
        $this->skurl    = $skurl;
    }

    /**
     * dispatch message
     *
     * @param Message   $message
     * @param integer   $user   receiver, the id
     * @param integer   $type   receiver, the type
     */
    public function dispatch(Message $message, $user, $type = self::TYPE_USER)
    {
        return $this->weave($this->skurl->post(
            "{$this->gateway}/v1/message/send",
            array_merge([
                'receiver_type' => $type,
                'receiver_id'   => $user
            ], $message->normalize())
        ));
    }

    /**
     * expose folks to user
     *
     * @param integer   $user   user, the id
     * @param array     $folks  (id, name) pair
     */
    public function expose($user, array $folks)
    {
        return $this->weave($this->skurl->post(
            "{$this->gateway}/v1/friend/recommend",
            [
                'data' => json_encode([
                    [
                        'user_id'   => $user,
                        'contacts'  => array_map(function ($pair) {
                            list($id, $name) = $pair;

                            return [
                                'id'        => $id,
                                'name'      => $name,
                                'mobile'    => null
                            ];
                        }, $folks)
                    ]
                ])
            ]
        ));
    }

    /**
     * dispatch service call
     *
     * @param array     $services   services, the user id
     * @param Message   $message
     * @param integer   $referer    referer, an dummy external unique id
     */
    public function challenge(array $services, Message $message, $referer)
    {
        return $this->weave($this->skurl->post(
            "{$this->gateway}/v1/temp/chat",
            array_merge([
                'reference_id'  => $referer,
                'receiver_ids'  => implode(',', $services)
            ], $message->normalize())
        ));
    }

    /**
     * set up a transient relationship
     *
     * @param integer   $user   user, the id
     * @param string    $token  token, of current user
     */
    public function touch($user, $token)
    {
        return $this->weave($this->skurl->post("{$this->gateway}/v1/temp/relation", [
            'user_id'   => $user,
            'token'     => $token
        ]));
    }

    private function weave($responseText)
    {
        $document = json_decode($responseText, true);
        if ($document === null)
            throw new Exception('cannot be decoded');

        if (!empty($document['error']))
            throw new Exception($document['message'], $document['error']);

        return $document;
    }
}
